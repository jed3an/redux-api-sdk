const _ = require("lodash");

const getApisArray = (jsonFile) => {
  const paths = _.map(jsonFile, (val, key) => {
    if (key === "paths") {
      return { key, value: val };
    }
  });
  const apisArray = [];
_.map(paths[2].value, (val, key) => {
    let methodsArray = [];
    _.map(val, (_value1, key) => {
      methodsArray.push(key);
return key;
    });
    apisArray.push({
      apiUrl: key,
      apiName: key.replace(/\\|\//g, ""),
      methods: methodsArray,
    });
    return { key, val };
  });
  return apisArray;
};

exports.getApisArray = getApisArray;
