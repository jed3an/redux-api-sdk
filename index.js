import rootReducer from './dist/sdk/reducers/index';
import rootLogics from './dist/sdk/logics/index';

export {
    rootReducer,
    rootLogics
}