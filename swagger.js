const data = {
  openapi: "3.0.1",
  info: {
    title: "Loans",
    version: "v1",
  },
  paths: {
    "/Client/Health": {
      get: {
        tags: ["Client"],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Client/CreateNewClient": {
      post: {
        tags: ["Client"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/AddNewClientRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/AddNewClientRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/AddNewClientRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Client/GetClientStatusList": {
      get: {
        tags: ["Client"],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Client/GetClientRankList": {
      get: {
        tags: ["Client"],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Client/GetClientRecruitingTypes": {
      get: {
        tags: ["Client"],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Client/UpdateClientData": {
      post: {
        tags: ["Client"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/UpdateClientDataRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/UpdateClientDataRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/UpdateClientDataRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Client/UpdateClientFirstPicture": {
      post: {
        tags: ["Client"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/UpdatePictureRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/UpdatePictureRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/UpdatePictureRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Client/UpdateClientSecondPicture": {
      post: {
        tags: ["Client"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/UpdatePictureRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/UpdatePictureRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/UpdatePictureRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Client/UpdateClientPassportPicture": {
      post: {
        tags: ["Client"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/UpdatePictureRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/UpdatePictureRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/UpdatePictureRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Client/GetClientById": {
      get: {
        tags: ["Client"],
        parameters: [
          {
            name: "clientId",
            in: "query",
            schema: {
              type: "string",
              format: "uuid",
              default: "",
            },
          },
        ],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Client/GetClientsList": {
      get: {
        tags: ["Client"],
        parameters: [
          {
            name: "pageIndex",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
          {
            name: "pageSize",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
        ],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Client/SearchClients": {
      get: {
        tags: ["Client"],
        parameters: [
          {
            name: "name",
            in: "query",
            schema: {
              type: "string",
              default: "",
            },
          },
          {
            name: "clientNumber",
            in: "query",
            schema: {
              type: "string",
              default: "",
            },
          },
          {
            name: "passport",
            in: "query",
            schema: {
              type: "string",
              default: "",
            },
          },
          {
            name: "pageIndex",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
          {
            name: "pageSize",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
        ],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Client/UpdateClientDebt": {
      post: {
        tags: ["Client"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/UpdateClientDebtRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/UpdateClientDebtRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/UpdateClientDebtRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/lawsuit/CourtPayments/CloseCourtPaymentsLS": {
      post: {
        tags: ["CourtPayments"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/BaseLawsuitRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/BaseLawsuitRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/BaseLawsuitRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/lawsuit/CourtPayments/CreateCourtPaymentsLS": {
      post: {
        tags: ["CourtPayments"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/BaseLawsuitRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/BaseLawsuitRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/BaseLawsuitRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/lawsuit/CourtPayments/GetCourtPaymentLSById": {
      get: {
        tags: ["CourtPayments"],
        parameters: [
          {
            name: "lawsuitId",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
        ],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/lawsuit/CourtPayments/PayCP": {
      post: {
        tags: ["CourtPayments"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/PayCPRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/PayCPRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/PayCPRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/lawsuit/CourtPayments/GetCourtPaymentsLS": {
      get: {
        tags: ["CourtPayments"],
        parameters: [
          {
            name: "pageIndex",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
          {
            name: "pageSize",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
        ],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Daily/StartRoutine": {
      post: {
        tags: ["Daily"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/DailyBaseRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/DailyBaseRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/DailyBaseRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Daily/EndRoutine": {
      post: {
        tags: ["Daily"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/DailyBaseRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/DailyBaseRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/DailyBaseRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Daily/AddExpense": {
      post: {
        tags: ["Daily"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/AddExpenseRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/AddExpenseRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/AddExpenseRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Daily/GetDailyExpenses": {
      get: {
        tags: ["Daily"],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Deals/Health": {
      get: {
        tags: ["Deals"],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Deals/CreateNewDeal": {
      post: {
        tags: ["Deals"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/CreateNewDealRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/CreateNewDealRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/CreateNewDealRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Deals/ExpandDeal": {
      post: {
        tags: ["Deals"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/ExpandDealRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/ExpandDealRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/ExpandDealRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Deals/GetClientDeals": {
      get: {
        tags: ["Deals"],
        parameters: [
          {
            name: "clientId",
            in: "query",
            schema: {
              type: "string",
              format: "uuid",
              default: "",
            },
          },
        ],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Deals/GetUserDeals": {
      get: {
        tags: ["Deals"],
        parameters: [
          {
            name: "username",
            in: "query",
            schema: {
              type: "string",
              default: "",
            },
          },
          {
            name: "pageIndex",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
          {
            name: "pageSize",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
        ],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Deals/GetAllDeals": {
      get: {
        tags: ["Deals"],
        parameters: [
          {
            name: "pageIndex",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
          {
            name: "pageSize",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
        ],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Deals/GetDealsInRange": {
      get: {
        tags: ["Deals"],
        parameters: [
          {
            name: "pageIndex",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
          {
            name: "pageSize",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
          {
            name: "startingDate",
            in: "query",
            schema: {
              type: "string",
              format: "date-time",
              default: "",
            },
          },
          {
            name: "endingDate",
            in: "query",
            schema: {
              type: "string",
              format: "date-time",
              default: "",
            },
          },
        ],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Deals/SearchDeals": {
      get: {
        tags: ["Deals"],
        parameters: [
          {
            name: "pageIndex",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
          {
            name: "pageSize",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
          {
            name: "fromDate",
            in: "query",
            schema: {
              type: "string",
              format: "date-time",
              default: "",
            },
          },
          {
            name: "toDate",
            in: "query",
            schema: {
              type: "string",
              format: "date-time",
              default: "",
            },
          },
          {
            name: "clientName",
            in: "query",
            schema: {
              type: "string",
              default: "",
            },
          },
          {
            name: "clientNumber",
            in: "query",
            schema: {
              type: "string",
              default: "",
            },
          },
          {
            name: "statusId",
            in: "query",
            schema: {
              type: "string",
              default: "",
            },
          },
          {
            name: "dealId",
            in: "query",
            schema: {
              type: "string",
              default: "",
            },
          },
        ],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/lawsuit/Foreclosure/CloseWarningLS": {
      post: {
        tags: ["Foreclosure"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/BaseLawsuitRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/BaseLawsuitRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/BaseLawsuitRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/lawsuit/Foreclosure/CreateForeclosureLS": {
      post: {
        tags: ["Foreclosure"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/ForeclosureLSRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/ForeclosureLSRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/ForeclosureLSRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/lawsuit/Foreclosure/GetForeclosureLSById": {
      get: {
        tags: ["Foreclosure"],
        parameters: [
          {
            name: "lawsuitId",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
        ],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/lawsuit/Foreclosure/GetForeclosureLS": {
      get: {
        tags: ["Foreclosure"],
        parameters: [
          {
            name: "pageSize",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
          {
            name: "pageIndex",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
        ],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/lawsuit/Foreclosure/UpdateForeclosureLS": {
      post: {
        tags: ["Foreclosure"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/ForeclosureLSRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/ForeclosureLSRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/ForeclosureLSRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Lawsuit/GetNewLawsuits": {
      get: {
        tags: ["Lawsuit"],
        parameters: [
          {
            name: "pageIndex",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
          {
            name: "pageSize",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
        ],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Lawsuit/OpenLawsuit": {
      post: {
        tags: ["Lawsuit"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/BaseClientRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/BaseClientRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/BaseClientRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Lawsuit/UpdateLawsuitNumber": {
      post: {
        tags: ["Lawsuit"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/UpdatelawsuitNumberRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/UpdatelawsuitNumberRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/UpdatelawsuitNumberRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/Lawsuit/SearchLawsuits": {
      get: {
        tags: ["Lawsuit"],
        parameters: [
          {
            name: "lawsuitTypeId",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
          {
            name: "lawsuitiId",
            in: "query",
            schema: {
              type: "string",
              default: "",
            },
          },
          {
            name: "lawsuitNumber",
            in: "query",
            schema: {
              type: "string",
              default: "",
            },
          },
          {
            name: "clientName",
            in: "query",
            schema: {
              type: "string",
              default: "",
            },
          },
          {
            name: "clientNumber",
            in: "query",
            schema: {
              type: "string",
              default: "",
            },
          },
          {
            name: "pageIndex",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
          {
            name: "pageSize",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
        ],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/lawsuit/PA/CreateCourtPaymentsLS": {
      post: {
        tags: ["PA"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/CreatePARequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/CreatePARequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/CreatePARequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/lawsuit/PA/GetPAPayments": {
      get: {
        tags: ["PA"],
        parameters: [
          {
            name: "paymentArrangmentId",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
        ],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/lawsuit/PA/GetPALawsuits": {
      get: {
        tags: ["PA"],
        parameters: [
          {
            name: "pageIndex",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
          {
            name: "pageSize",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
        ],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/lawsuit/PA/PayPAPayment": {
      post: {
        tags: ["PA"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/PayPAPaymentRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/PayPAPaymentRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/PayPAPaymentRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/PaymentCollection/GetMoneyTypes": {
      get: {
        tags: ["PaymentCollection"],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/PaymentCollection/CollectPayment": {
      post: {
        tags: ["PaymentCollection"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/PaymentCollectionBaseRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/PaymentCollectionBaseRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/PaymentCollectionBaseRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/PaymentCollection/CancelPayment": {
      post: {
        tags: ["PaymentCollection"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/PaymentCollectionBaseRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/PaymentCollectionBaseRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/PaymentCollectionBaseRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/PaymentCollection/FineClient": {
      post: {
        tags: ["PaymentCollection"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/FineDealRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/FineDealRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/FineDealRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/PaymentCollection/GetAllPayments": {
      get: {
        tags: ["PaymentCollection"],
        parameters: [
          {
            name: "pageIndex",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
          {
            name: "pageSize",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
        ],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/PaymentCollection/GetClientPayments": {
      get: {
        tags: ["PaymentCollection"],
        parameters: [
          {
            name: "clientId",
            in: "query",
            schema: {
              type: "string",
              format: "uuid",
              default: "",
            },
          },
          {
            name: "pageIndex",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
          {
            name: "pageSize",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
        ],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/PaymentCollection/GetDealPayments": {
      get: {
        tags: ["PaymentCollection"],
        parameters: [
          {
            name: "dealId",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
        ],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/PaymentCollection/GetUserPayments": {
      get: {
        tags: ["PaymentCollection"],
        parameters: [
          {
            name: "username",
            in: "query",
            schema: {
              type: "string",
              default: "",
            },
          },
          {
            name: "pageIndex",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
          {
            name: "pageSize",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
        ],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/PaymentCollection/GetUpcomingPayments": {
      get: {
        tags: ["PaymentCollection"],
        parameters: [
          {
            name: "pageIndex",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
          {
            name: "pageSize",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
        ],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/PaymentCollection/GetUserRoutinePayment": {
      get: {
        tags: ["PaymentCollection"],
        parameters: [
          {
            name: "pageIndex",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
          {
            name: "pageSize",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
          {
            name: "username",
            in: "query",
            schema: {
              type: "string",
              default: "",
            },
          },
        ],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/PaymentCollection/GetPaymentsInRange": {
      get: {
        tags: ["PaymentCollection"],
        parameters: [
          {
            name: "startingDate",
            in: "query",
            schema: {
              type: "string",
              format: "date-time",
              default: "",
            },
          },
          {
            name: "endingDate",
            in: "query",
            schema: {
              type: "string",
              format: "date-time",
              default: "",
            },
          },
          {
            name: "pageIndex",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
          {
            name: "pageSize",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
        ],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/PaymentCollection/GetMoneyOverView": {
      get: {
        tags: ["PaymentCollection"],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/User/Login": {
      post: {
        tags: ["User"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/LoginRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/LoginRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/LoginRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/User/Health": {
      get: {
        tags: ["User"],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/User/RefreshToken": {
      post: {
        tags: ["User"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/RefreshTokenRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/RefreshTokenRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/RefreshTokenRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/User/Register": {
      post: {
        tags: ["User"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/RegisterNewUserRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/RegisterNewUserRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/RegisterNewUserRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/User/SendMessage": {
      post: {
        tags: ["User"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/SendMessageRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/SendMessageRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/SendMessageRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/User/RetrieveMessages": {
      get: {
        tags: ["User"],
        parameters: [
          {
            name: "receiver",
            in: "query",
            schema: {
              type: "string",
              default: "",
            },
          },
        ],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/User/GetUsers": {
      get: {
        tags: ["User"],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/lawsuit/WarningLS/CloseWarningLS": {
      post: {
        tags: ["WarningLS"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/BaseLawsuitRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/BaseLawsuitRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/BaseLawsuitRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/lawsuit/WarningLS/CreateWarningLS": {
      post: {
        tags: ["WarningLS"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/WarningLSRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/WarningLSRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/WarningLSRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/lawsuit/WarningLS/GetWarningLSById": {
      get: {
        tags: ["WarningLS"],
        parameters: [
          {
            name: "lawsuitId",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
        ],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/lawsuit/WarningLS/GetTwoMonthsLS": {
      get: {
        tags: ["WarningLS"],
        parameters: [
          {
            name: "pageIndex",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
          {
            name: "pageSize",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
        ],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/lawsuit/WarningLS/GetWarningLS": {
      get: {
        tags: ["WarningLS"],
        parameters: [
          {
            name: "pageIndex",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
          {
            name: "pageSize",
            in: "query",
            schema: {
              type: "integer",
              format: "int32",
            },
          },
        ],
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
    "/lawsuit/WarningLS/UpdateWarningLS": {
      post: {
        tags: ["WarningLS"],
        requestBody: {
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/WarningLSRequest",
              },
            },
            "text/json": {
              schema: {
                $ref: "#/components/schemas/WarningLSRequest",
              },
            },
            "application/*+json": {
              schema: {
                $ref: "#/components/schemas/WarningLSRequest",
              },
            },
          },
        },
        responses: {
          200: {
            description: "Success",
          },
        },
      },
    },
  },
  components: {
    schemas: {
      ImageModel: {
        type: "object",
        properties: {
          imageId: {
            type: "string",
          },
          imageData: {
            type: "string",
          },
          imageType: {
            type: "string",
          },
          oldImageType: {
            type: "string",
          },
        },
        additionalProperties: false,
      },
      AddNewClientRequest: {
        type: "object",
        properties: {
          passportImage: {
            $ref: "#/components/schemas/ImageModel",
          },
          firstPicture: {
            $ref: "#/components/schemas/ImageModel",
          },
          secondPicture: {
            $ref: "#/components/schemas/ImageModel",
          },
          clientNumber: {
            type: "string",
          },
          firstName: {
            type: "string",
          },
          fullName: {
            type: "string",
          },
          fullNameEng: {
            type: "string",
          },
          passportOld: {
            type: "string",
          },
          passportNew: {
            type: "string",
          },
          firstPhoneNumber: {
            type: "string",
          },
          secondPhoneNumber: {
            type: "string",
          },
          thirdPhoneNumber: {
            type: "string",
          },
          originalCountry: {
            type: "string",
          },
          originalCountryPhone: {
            type: "string",
          },
          email: {
            type: "string",
          },
          facebook: {
            type: "string",
          },
          recruiteTypeId: {
            type: "integer",
            format: "int32",
          },
          recruiterClientId: {
            type: "string",
            format: "uuid",
          },
          city: {
            type: "string",
          },
          workAddress: {
            type: "string",
          },
          firstHomeAddress: {
            type: "string",
          },
          secondHomeAddress: {
            type: "string",
          },
          postBankNumber: {
            type: "string",
          },
          fatherNameHebrew: {
            type: "string",
          },
          birthday: {
            type: "string",
            format: "date-time",
          },
          employerName: {
            type: "string",
          },
          employerPhone: {
            type: "string",
          },
          relativeName: {
            type: "string",
          },
          relativePhone: {
            type: "string",
          },
          comments: {
            type: "string",
          },
          obligo: {
            type: "number",
            format: "double",
          },
          statusId: {
            type: "integer",
            format: "int32",
          },
          ratingId: {
            type: "integer",
            format: "int32",
          },
          address: {
            type: "string",
          },
        },
        additionalProperties: false,
      },
      UpdateClientDataRequest: {
        type: "object",
        properties: {
          clientId: {
            type: "string",
            format: "uuid",
          },
          clientNumber: {
            type: "string",
          },
          firstName: {
            type: "string",
          },
          fullName: {
            type: "string",
          },
          fullNameEng: {
            type: "string",
          },
          passportOld: {
            type: "string",
          },
          passportNew: {
            type: "string",
          },
          firstPhoneNumber: {
            type: "string",
          },
          secondPhoneNumber: {
            type: "string",
          },
          thirdPhoneNumber: {
            type: "string",
          },
          originalCountry: {
            type: "string",
          },
          originalCountryPhone: {
            type: "string",
          },
          email: {
            type: "string",
          },
          facebook: {
            type: "string",
          },
          recruiteTypeId: {
            type: "integer",
            format: "int32",
          },
          recruiterClientId: {
            type: "string",
            format: "uuid",
          },
          city: {
            type: "string",
          },
          workAddress: {
            type: "string",
          },
          firstHomeAddress: {
            type: "string",
          },
          secondHomeAddress: {
            type: "string",
          },
          postBankNumber: {
            type: "string",
          },
          fatherNameHebrew: {
            type: "string",
          },
          birthday: {
            type: "string",
            format: "date-time",
          },
          employerName: {
            type: "string",
          },
          employerPhone: {
            type: "string",
          },
          relativeName: {
            type: "string",
          },
          relativePhone: {
            type: "string",
          },
          comments: {
            type: "string",
          },
          obligo: {
            type: "number",
            format: "double",
          },
          statusId: {
            type: "integer",
            format: "int32",
          },
          ratingId: {
            type: "integer",
            format: "int32",
          },
          address: {
            type: "string",
          },
        },
        additionalProperties: false,
      },
      UpdatePictureRequest: {
        type: "object",
        properties: {
          clientId: {
            type: "string",
            format: "uuid",
          },
          image: {
            $ref: "#/components/schemas/ImageModel",
          },
        },
        additionalProperties: false,
      },
      UpdateClientDebtRequest: {
        type: "object",
        properties: {
          newDebt: {
            type: "number",
            format: "double",
          },
          clientId: {
            type: "string",
            format: "uuid",
          },
        },
        additionalProperties: false,
      },
      BaseLawsuitRequest: {
        type: "object",
        properties: {
          lawsuitId: {
            type: "integer",
            format: "int32",
          },
        },
        additionalProperties: false,
      },
      PayCPRequest: {
        type: "object",
        properties: {
          amount: {
            type: "number",
            format: "double",
          },
          lawsuitId: {
            type: "integer",
            format: "int32",
          },
        },
        additionalProperties: false,
      },
      DailyBaseRequest: {
        type: "object",
        properties: {
          amount: {
            type: "number",
            format: "double",
          },
        },
        additionalProperties: false,
      },
      AddExpenseRequest: {
        type: "object",
        properties: {
          comments: {
            type: "string",
          },
          amount: {
            type: "number",
            format: "double",
          },
        },
        additionalProperties: false,
      },
      CreateNewDealRequest: {
        type: "object",
        properties: {
          clientId: {
            type: "string",
            format: "uuid",
          },
          firstPaymentDate: {
            type: "string",
            format: "date-time",
          },
          total: {
            type: "number",
            format: "double",
          },
          monthlyPaymentAmount: {
            type: "number",
            format: "double",
          },
          numberOfPayments: {
            type: "integer",
            format: "int32",
          },
          comments: {
            type: "string",
          },
        },
        additionalProperties: false,
      },
      ExpandDealRequest: {
        type: "object",
        properties: {
          parentDealId: {
            type: "integer",
            format: "int32",
          },
          clientId: {
            type: "string",
            format: "uuid",
          },
          firstPaymentDate: {
            type: "string",
            format: "date-time",
          },
          total: {
            type: "number",
            format: "double",
          },
          monthlyPaymentAmount: {
            type: "number",
            format: "double",
          },
          numberOfPayments: {
            type: "integer",
            format: "int32",
          },
          comments: {
            type: "string",
          },
        },
        additionalProperties: false,
      },
      ForeclosureLSRequest: {
        type: "object",
        properties: {
          lawsuitId: {
            type: "integer",
            format: "int32",
          },
          ongoing: {
            type: "boolean",
          },
          comments: {
            type: "string",
          },
        },
        additionalProperties: false,
      },
      BaseClientRequest: {
        type: "object",
        properties: {
          clientId: {
            type: "string",
            format: "uuid",
          },
        },
        additionalProperties: false,
      },
      UpdatelawsuitNumberRequest: {
        type: "object",
        properties: {
          lawsuitNumber: {
            type: "integer",
            format: "int32",
          },
          lawsuitOpenDate: {
            type: "string",
            format: "date-time",
          },
          lawsuitId: {
            type: "integer",
            format: "int32",
          },
        },
        additionalProperties: false,
      },
      CreatePARequest: {
        type: "object",
        properties: {
          total: {
            type: "number",
            format: "double",
          },
          monthlyPaymentAmount: {
            type: "number",
            format: "double",
          },
          numberOfPayments: {
            type: "integer",
            format: "int32",
          },
          firstPaymentDate: {
            type: "string",
            format: "date-time",
          },
          comments: {
            type: "string",
          },
          lawsuitId: {
            type: "integer",
            format: "int32",
          },
        },
        additionalProperties: false,
      },
      PayPAPaymentRequest: {
        type: "object",
        properties: {
          paymentId: {
            type: "integer",
            format: "int32",
          },
          amount: {
            type: "number",
            format: "double",
          },
        },
        additionalProperties: false,
      },
      PaymentCollectionBaseRequest: {
        type: "object",
        properties: {
          paymentId: {
            type: "integer",
            format: "int32",
          },
          moneyTypeId: {
            type: "integer",
            format: "int32",
          },
          amount: {
            type: "number",
            format: "double",
          },
          comments: {
            type: "string",
          },
        },
        additionalProperties: false,
      },
      FineDealRequest: {
        type: "object",
        properties: {
          dealId: {
            type: "integer",
            format: "int32",
          },
          amount: {
            type: "number",
            format: "double",
          },
          comments: {
            type: "string",
          },
          isCollected: {
            type: "boolean",
          },
        },
        additionalProperties: false,
      },
      LoginRequest: {
        type: "object",
        properties: {
          username: {
            type: "string",
          },
          password: {
            type: "string",
          },
        },
        additionalProperties: false,
      },
      RefreshTokenRequest: {
        type: "object",
        properties: {
          token: {
            type: "string",
          },
          refreshToken: {
            type: "string",
          },
        },
        additionalProperties: false,
      },
      RegisterNewUserRequest: {
        type: "object",
        properties: {
          username: {
            type: "string",
          },
          password: {
            type: "string",
          },
          email: {
            type: "string",
          },
          firstName: {
            type: "string",
          },
          lastName: {
            type: "string",
          },
          phone: {
            type: "string",
          },
          userTypeId: {
            type: "integer",
            format: "int32",
          },
        },
        additionalProperties: false,
      },
      SendMessageRequest: {
        type: "object",
        properties: {
          sender: {
            type: "string",
          },
          receiver: {
            type: "string",
          },
          message: {
            type: "string",
          },
        },
        additionalProperties: false,
      },
      WarningLSRequest: {
        type: "object",
        properties: {
          seen: {
            type: "boolean",
          },
          deliveryDate: {
            type: "string",
            format: "date-time",
            nullable: true,
          },
          lawsuitId: {
            type: "integer",
            format: "int32",
          },
        },
        additionalProperties: false,
      },
    },
  },
};

exports.data = data;
