var _ = require("lodash");
const util = require("./ApisUtils");
const fs = require("fs");
const { identity } = require("lodash");
const yeoman = require("yeoman-environment");
const env = yeoman.createEnv();
const { data } = require("./swagger");
try {
  // require may still be needed for dynamic imports
  const apis = util.getApisArray(data);

  if (apis && apis.length > 0) {
    for (let index = 0; index < apis.length; index++) {
      const apiEntry = apis[index];

      for (let i = 0; i < apiEntry["methods"].length; i++) {
        const element = {
          apiUrl: apiEntry.apiUrl.toLowerCase(),
          apiName: apiEntry.apiName,
          apiMethod: apiEntry["methods"][i],
        };
        runGenerator(element);
      }
    }
  }
} catch (err) {
  console.log(err);
}

function runGenerator(args) {
  return new Promise((resolve, reject) => {
    env.lookup(() => {
      const defaulrArgumnets = {
        silent: true,
        "skip-install": true,
        skipInstall: true,
        custom: " no",
        "skip-cache": true,
        skipCache: true,
        overwrite: true,
        overwrite: "overwrite",
      };
      env.run("redux-logic-api", { ...defaulrArgumnets, ...args }, (err) => {
        if (err) {
          reject(err);
        }
        resolve();
      });
    });
  });
}
